const data = require("../js_drill_2.cjs");
function problem1() {
  try {
    let webDevelopers = [];
    for (let record of data) {
      if (record.job.startsWith("Web Developer"))
        webDevelopers.push(JSON.stringify(record));
    }
    console.log(webDevelopers);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem1;
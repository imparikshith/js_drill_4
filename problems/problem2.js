let data = require("../js_drill_2.cjs");

function problem2() {
  try {
    for (let record of data) {
      record.salary = parseFloat(record.salary.replace("$", ""));
    }
    console.log(data);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem2;
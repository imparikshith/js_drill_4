let data = require("../js_drill_2.cjs");

function problem3() {
  try {
    for (let record of data) {
      record.salary = parseFloat(record.salary.replace("$", "")) * 10000;
    }
    return data;
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem3;
const problem3 = require("./problem3.js");
const data = problem3();
function problem4() {
  try {
    let sum = 0;
    for (let record of data) {
      sum += record.salary;
    }
    sum /= 10000;
    console.log(`Sum of all the salaries if ${sum}`);
  } catch (error) {}
}
module.exports = problem4;
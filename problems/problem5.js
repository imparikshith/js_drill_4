const problem3 = require("./problem3");
const data = problem3();

function problem5() {
  try {
    let salaryByCountry = {};
    for (let record of data) {
      if (salaryByCountry[record.location]) {
        salaryByCountry[record.location] =
          (salaryByCountry[record.location] * 10000 + record.salary) / 10000;
      } else {
        salaryByCountry[record.location] = record.salary / 10000;
      }
    }
    return salaryByCountry;
  } catch (error) {}
}
module.exports = problem5;
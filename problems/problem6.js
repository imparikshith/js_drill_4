const problem5 = require("./problem5.js");
const data = require("../js_drill_2.cjs");
const salaryByCountry = problem5();

function problem6() {
  try {
    let employeeByCountry = {};
    for (let record of data) {
      if (employeeByCountry[record.location]) {
        employeeByCountry[record.location]++;
      } else {
        employeeByCountry[record.location] = 1;
      }
    }
    let avgSalaryByCountry = {};
    for (let country in salaryByCountry) {
      avgSalaryByCountry[country] =
        salaryByCountry[country] / employeeByCountry[country];
    }
    return avgSalaryByCountry;
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem6;